#!/usr/bin/env bash

##
## Copyright (c) 2022 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud/ for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##

#
# This script tries to follow the styleguide from
# https://google.github.io/styleguide/shellguide.html
#

# -e Fail on all uncatched errors
# -u Fails when variables are used before initialization
# -o Uncatched errors must not be swallowed by pipes
set -euo pipefail

# Activate tracing if debugging variable is provided
if [[ "${DEBUG:-0}" -gt "0" ]]; then
  set -x
fi

mkdir -p /var/run/ovn

# This is the ID of the agent in neutron(chassis in southbound)
ovs-vsctl set Open_vSwitch . external-ids:system-id="${HOSTNAME}"
ovs-vsctl set Open_vSwitch . external-ids:ovn-remote="${SOUTHBOUND_SERVERS}"
ovs-vsctl set Open_vSwitch . external-ids:ovn-remote-probe-interval="${SOUTHBOUND_INACTIVITY_PROBE}"
ovs-vsctl set Open_vSwitch . external-ids:ovn-encap-type=geneve
ovs-vsctl set Open_vSwitch . external-ids:ovn-encap-ip="${OVERLAY_IP_ADDRESS}"
ovs-vsctl set Open_vSwitch . external-ids:ovn-monitor-all="${OVN_MONITOR_ALL}"

if [ "${NETWORK_NODE:-false}" == 'true' ]; then
  bridge_mappings=()
  while read -r map || [ "${map}" ]; do
    bridge=$(echo "${map}" | cut -d \; -f 1)
    iface=$(echo "${map}" | cut -d \; -f 2)
    physnet=$(echo "${map}" | cut -d \; -f 3)
    echo "Creating Bridge ${bridge}"
    ovs-vsctl --db=unix:/run/openvswitch/db.sock --may-exist add-br "${bridge}"
    if [[ $iface ]]; then
      echo "Connecting Bridge ${bridge} to interface ${iface}"
      ovs-vsctl --db=unix:/run/openvswitch/db.sock --may-exist add-port "${bridge}" "${iface}"
      echo "Setting interface ${iface} to up"
      ip link set dev "${iface}" up
    else
      echo "Setting interface ${bridge} to up"
      ip link set dev "${bridge}" up
    fi
    bridge_mappings+=("${physnet}":"${bridge}")
    echo "Done with Bridge ${bridge}"
  done < /etc/neutron/bridge_mappings

  bm_concatenated=$(IFS=, ; echo "${bridge_mappings[*]}")
  echo "Adding bridge mappings ${bm_concatenated} in OVN"
  ovs-vsctl set Open_vSwitch . external-ids:ovn-bridge-mappings="${bm_concatenated}"

  ovs-vsctl set Open_vSwitch . external-ids:ovn-cms-options=enable-chassis-as-gw
  echo "Node is successfully set as network node"
fi

echo "External-ids record:"
ovs-vsctl get Open_vSwitch . external-ids
