#!/usr/bin/env bash
#./northd_startup.sh

##
## Copyright (c) 2022 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud/ for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##

set -e
set -u
set -o pipefail

#
# startup script for northd
# you must provide the following arguments to this script
#
# first argument: ovsdb connections to the northbound replica(s), using ssl:
# example:
# ssl:neutron-northbound-9plp4-ovsdb-0-pod.yaook.svc.cluster.local:6641
#
# first argument: ovsdb connections to the southbound replica(s), using ssl:
# example:
# ssl:neutron-southbound-tqs4b-ovsdb-0-pod.yaook.svc.cluster.local:6642
#
# Optionally set ALERT_THRESHOLD_SECONDS env var to modify the alert timer.
#

#
# This startup script for northd first checks the schema versions of the
# northbound and southbound databases against the .ovsschema files provided
# with this image. The databases will be upgraded if their schema is older
# than the schema files, but newer schemas will not be downgraded.
# northd will be started after the schemas are up-to-date.
#

if test -n "${DEBUG:-}"; then
  set -x
fi

OVSDB_AUTH_ARGS=("--private-key=${TLS_KEY_FILENAME:-/etc/ssl/private/tls.key}"
                 "--certificate=${TLS_CRT_FILENAME:-/etc/ssl/private/tls.crt}"
                 "--ca-cert=${CA_CRT_FILENAME:-/etc/ssl/private/ca.crt}")

eerror() {
  if test -z "${DISABLE_EERROR_OUTPUT:-}"; then
    echo "# ERROR: $*"
  fi

  if test -z "${DISABLE_EERROR_AUTOMATIC_EXIT:-}"; then
    exit 1
  fi
}

ewarn() {
  if test -z "${DISABLE_EWARN_OUTPUT:-}"; then
    echo "# WARNING: $*"
  fi
}

einfo() {
  if test -z "${DISABLE_EINFO_OUTPUT:-}"; then
    echo "# INFO: $*"
  fi
}

conditional_schema_update() {
  local connection
  local schema_file

  local db_name
  local db_version
  local schema_version

  connection="$1"
  schema_file="$2"

  db_name="$(jq -r '.name' "$schema_file")"
  db_version="$(ovsdb-client "${OVSDB_AUTH_ARGS[@]}" \
                  get-schema-version "$connection" "$db_name")"
  schema_version="$(jq -r '.version' "$schema_file")"

  if dpkg --compare-versions "$db_version" eq "$schema_version"; then
    einfo "${db_name} schema is up-to-date"
  elif dpkg --compare-versions "$db_version" gt "$schema_version"; then
    ewarn "${db_name} schema is more recent than this image"
  else
    einfo "Updating ${db_name} schema to version ${schema_version}"
    ovsdb-client "${OVSDB_AUTH_ARGS[@]}" convert "$connection" "$schema_file"
  fi
}

main() {
  local northbound_db
  local southbound_db

  northbound_db="${1:-}"
  southbound_db="${2:-}"

  if test -z "$northbound_db"; then
    eerror "Argument missing: northbound DB"
  fi

  if test -z "$southbound_db"; then
    eerror "Argument missing: southbound DB"
  fi

  conditional_schema_update "$northbound_db" /usr/share/ovn/ovn-nb.ovsschema
  conditional_schema_update "$southbound_db" /usr/share/ovn/ovn-sb.ovsschema

  threads="${NORTHD_THREADS:-1}"

  ovn-northd \
    "${OVSDB_AUTH_ARGS[@]}" \
    --ovnnb-db="$northbound_db" \
    --ovnsb-db="$southbound_db" \
    --pidfile=/run/ovn/ovn-northd.pid \
    -vconsole:off \
    -vfile:info \
    --log-file=/dev/stdout \
    --n-threads="${threads}"
}

main "$@"

