#!/usr/bin/env bash

##
## Copyright (c) 2022 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud/ for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##

#
# This script tries to follow the styleguide from
# https://google.github.io/styleguide/shellguide.html
#

# -e Fail on all uncatched errors
# -u Fails when variables are used before initialization
# -o Uncatched errors must not be swallowed by pipes
set -euo pipefail

# Activate tracing if debugging variable is provided
if [[ "${DEBUG:-0}" -gt "0" ]]; then
  set -x
fi

FQDN="${MY_POD_NAME}.${HEADLESS_SERVICE_FQDN}"

if [[ "${STANZA}" == "nb" ]]; then
  PORT="6641"
else
  PORT="6642"
fi

intro() {
  echo "MY_POD_IP variable is: ${MY_POD_IP}"
  echo "MY_POD_NAME variable is: ${MY_POD_NAME}"
  echo "FQDN variable is: ${FQDN}"
}

preflight() {
  if test -f "${INIT_CONTAINER_FINISHED_LOCKFILE}"; then
    echo "My init container work was successfully completed before, exiting."
    exit 0
  fi
}

__check_if_cluster_already_exists() {
  curl -m 1 -o /dev/null "${HEADLESS_SERVICE_FQDN}:${PORT}"
  echo $?
}

__find_active_cluster_member() {
  echo "ovsdb-client "\
    "--private-key=/etc/ssl/private/tls.key" \
    "--certificate=/etc/ssl/private/tls.crt" \
    "--ca-cert=/etc/ssl/private/ca.crt" \
    "list-dbs" \
    "ssl:${1}:${PORT}" | tee /dev/stderr | bash > /dev/null

}

__run_ovn_ctl() {
  echo timeout 60 /usr/share/ovn/scripts/ovn-ctl \
    "--ovn-${STANZA}-db-ssl-key=/etc/ssl/private/tls.key" \
    "--ovn-${STANZA}-db-ssl-cert=/etc/ssl/private/tls.crt" \
    "--ovn-${STANZA}-db-ssl-ca-cert=/etc/ssl/private/ca.crt" \
    "--db-${STANZA}-addr=${FQDN}" \
    "--db-${STANZA}-cluster-local-addr=${FQDN}" \
    "--db-${STANZA}-cluster-local-proto=ssl" \
    "${@}" \
      "start_${STANZA}_ovsdb" | tee /dev/stderr | bash
}

__setup_tls() {
  SSL_DIR="${SSL_DIR:-/etc/ssl/private}"
  echo "Configuring TLS certificates."
  echo "ovn-${STANZA}ctl" set-ssl \
    "${SSL_DIR}/tls.key" \
    "${SSL_DIR}/tls.crt" \
    "${SSL_DIR}/ca.crt" | tee /dev/stderr | bash
}

_setup_leader() {
  echo "Setting up initial cluster in pod ${MY_POD_NAME} (${FQDN})".
  __run_ovn_ctl
  __setup_tls
}

_disaster_recovery() {
  echo "Revive the cluster through pod ${MY_POD_NAME} (${FQDN})".
  echo "rm -f ${INIT_CONTAINER_FINISHED_LOCKFILE}" | tee /dev/stderr | bash
  echo "cp /etc/ovn/ovn${STANZA}_db.db /etc/ovn/ovn${STANZA}-db.db-bk-$(date --iso-8601=seconds)" | tee /dev/stderr | bash
  echo "ovsdb-tool cluster-to-standalone /etc/ovn/ovn${STANZA}_db.db-sa /etc/ovn/ovn${STANZA}_db.db" | tee /dev/stderr | bash
  echo "cp /etc/ovn/ovn${STANZA}_db.db-sa /etc/ovn/ovn${STANZA}_db.db" | tee /dev/stderr | bash
  __run_ovn_ctl
  __setup_tls
  echo "rm /etc/ovn/ovn${STANZA}_db.db-sa" | tee /dev/stderr | bash
}

_setup_follower() {
  echo "Setting up cluster follower in pod ${MY_POD_NAME} (${FQDN})."
  for ((i=0; i<CLUSTER_SIZE; i++)); do
    JOIN_POD_FQDN="${FIRST_POD_FQDN//ovsdb-0/ovsdb-$i}"
    if [[ "${JOIN_POD_FQDN}" == "$FQDN" ]]; then
      continue
    fi
    if __find_active_cluster_member "${JOIN_POD_FQDN}"; then
      __run_ovn_ctl \
        "--db-${STANZA}-cluster-remote-addr=${JOIN_POD_FQDN}" \
        "--db-${STANZA}-cluster-remote-proto=ssl"
      break
    fi
  done
}

_stop_ovsdb_server() {
  echo Shutting down ovsdb-server inside init container.
  while true; do
    if pidof ovsdb-server 1>/dev/null; then
      kill -HUP "$(pidof ovsdb-server)"
    else
      break
    fi

    sleep 1
  done
}

epilogue() {
  echo Setting up permissions of OVN directories.
  chown -Rv "2500016:nogroup" /etc/ovn /var/run/ovn
  ls -aliR /etc/ovn /var/run/ovn

  LANG=C date >"${INIT_CONTAINER_FINISHED_LOCKFILE}"
  echo "Init container has completed successfully."
  echo "Statefile in ${INIT_CONTAINER_FINISHED_LOCKFILE}"
}

main() {
  intro
  ADDITIONAL_PARAMS=${1:-}
  if [[ -z "${ADDITIONAL_PARAMS}" ]]; then
    preflight
    # This checks for the integer ordinal 0 (first pod in statefulset).
    if [[ "${MY_POD_NAME##*-}" == "0" && $(__check_if_cluster_already_exists) != "0" ]]; then
      _setup_leader
    else
      _setup_follower
    fi
  else
    if [[ "${ADDITIONAL_PARAMS}" == "disaster-recovery" ]]; then
      _disaster_recovery
    elif [[ "${ADDITIONAL_PARAMS}" == "following-after-disaster" ]]; then
      _setup_follower
    fi
  fi

  _stop_ovsdb_server

  epilogue
}

main "$@"
