import subprocess
import time
import os
import logging
import sys

logger = logging.getLogger(__name__)
db_name = os.environ.get("DBNAME")
fqdn = os.environ.get("HOSTNAME")
dbctl = os.environ.get("DBCTL")
duplicate_id = None
member_id = None
cluster_info_cmd = None
get_cluster_info = ["ovs-appctl", "-t", dbctl, "cluster/status", db_name]
while True:
    try:
        cluster_info_cmd = subprocess.check_output(get_cluster_info)
        break
    except subprocess.CalledProcessError:
        logger.info("The command does not yet return its output, please try"
                    "again in 30s ")
        time.sleep(30)


cluster_info = cluster_info_cmd.decode()
cluster_info_split = cluster_info.splitlines()

for entry in cluster_info_split:
    if "Server ID" in entry:
        member_id = entry[11:15]
if member_id is None:
    logger.error("No Member Id found. This is impossible unless the output"
                 " format changed significantly")
    sys.exit(1)

for line in reversed(cluster_info_split):
    if "Servers" in line:
        break
    if fqdn in line and member_id not in line:
        duplicate_id = line[4:9]
        kick = ["ovs-appctl", "-t", dbctl, "cluster/kick",
                db_name, duplicate_id]
        try:
            subprocess.run(kick)
        except subprocess.CalledProcessError as e:
            logger.error(f"Kicking node {duplicate_id} from {fqdn} threw {e}")
            sys.exit(1)
        finally:
            logger.info(f"Node {duplicate_id} has been successfully removed")
    else:
        logger.info("There is currently no duplicate inside the cluster")
