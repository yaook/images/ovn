#!/usr/bin/env bash
#./northd_livenessprobe.sh

##
## Copyright (c) 2022 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud/ for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##

set -e
set -u
set -o pipefail

#
# liveness probe for northd running
# you must provide the following arguments to this script
#
# first argument: single ovsdb-server running northbound database,
# preceeded with ssl:
# example:
# ssl:neutron-northbound-9plp4-ovsdb-0-pod.yaook.svc.cluster.local:6641
#
# second argument: single ovsdb-server running southbound database,
# preceeded with ssl:
# example:
# ssl:neutron-southbound-tqs4b-ovsdb-0-pod.yaook.svc.cluster.local:6642
#
# Optionally set ALERT_THRESHOLD_SECONDS env var to modify the alert timer.
#

#
# The liveness probe script for northd works the following way:
# It uses a timestamp file (typically in /tmp) that gets updated as
# long as everything is nice.
#
# Nice in this case means nb_id from northbound and southbound are
# identical.
#
# If they are not identical we do not update the timestamp file.
#
# If the timestamp file is not updated for a while,
# (for example because the IDs never are the same for a long time)
# then the check kicks in and alerts by exiting 1 in the liveness probe.
#
# It will subtract the current unixtime value from the value stored in the
# outdated timestamp file. This will then be compared to the threshold
# seconds value. If it is greater that means the following:
# In the last ALERT_THRESHOLD_SECONDS timeframe there has never been at
# least ONE occasion where the liveness probe found nb_id in southbound
# and northbound database to be equal.
#

if test -n "${DEBUG:-}"; then
  set -x
fi

# 2 hours is default
ALERT_THRESHOLD_SECONDS="${ALERT_THRESHOLD_SECONDS:-$(( 2 * 60 * 60 ))}"

_prepare_select_json() {
  cat <<EOF
[
  "OVN_${1:-Northbound}", {
    "op":"select", "table":"${2:-NB}_Global", "where": []
  }
]
EOF
}

_get_nb_cfg_value() {
  ovsdb-client \
    --private-key="${TLS_KEY_FILENAME:-/etc/ssl/private/tls.key}" \
    --certificate="${TLS_CRT_FILENAME:-/etc/ssl/private/tls.crt}" \
    --ca-cert="${CA_CRT_FILENAME:-/etc/ssl/private/ca.crt}" \
      transact "$1" "$2" | jq -r '.[] | .rows | .[] | .nb_cfg'
}

update_northbound_nb_id() {
  _get_nb_cfg_value "$1" "$(_prepare_select_json)"
}

update_southbound_nb_id() {
  _get_nb_cfg_value "$1" "$(_prepare_select_json "Southbound" "SB")"
}

eerror() {
  if test -z "${DISABLE_EERROR_OUTPUT:-}"; then
    echo "# ERROR: $*"
  fi

  if test -z "${DISABLE_EERROR_AUTOMATIC_EXIT:-}"; then
    exit 1
  fi
}

ewarn() {
  if test -z "${DISABLE_EWARN_OUTPUT:-}"; then
    echo "# WARNING: $*"
  fi
}

einfo() {
  if test -z "${DISABLE_EINFO_OUTPUT:-}"; then
    echo "# INFO: $*"
  fi
}

main() {
  local northbound_db
  local southbound_db

  local northbound_nb_id
  local southbound_nb_id

  local now

  local alock
  local old_alock

  northbound_db="$1"
  southbound_db="$2"

  now="$(date +%s)"

  alock="${NORTHD_ALERT_LOCK:-/tmp/northd_alert_timer.txt}"
  touch "${alock}"

  old_alock="$(cat "${alock}")"
  # initialize this var only if its empty at the beginning of the script
  old_alock="${old_alock:-$now}"

  if test -z "$northbound_db"; then
    eerror "Argument missing: northbound DB"
  fi

  if test -z "$southbound_db"; then
    eerror "Argument missing: southbound DB"
  fi

  northbound_nb_id="$(update_northbound_nb_id "$northbound_db")"
  southbound_nb_id="$(update_southbound_nb_id "$southbound_db")"

  einfo "Current northbound nb_id: $northbound_nb_id"
  einfo "Current southbound nb_id: $southbound_nb_id"

  if [[ "$northbound_nb_id" == "$southbound_nb_id" ]]; then
    einfo "Updating ${alock} timestamp file: ${now}."
    echo "$now" >"${alock}"
    exit 0
  fi

  if [[ "$(( now - old_alock ))" -gt "${ALERT_THRESHOLD_SECONDS}" ]]; then
    ewarn "The NB and SB nb_id has been different for too long."
    exit 1
  fi
}

main "$@"

