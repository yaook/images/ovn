##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
ARG ovn_version
ARG ovs_version
ARG make_parallelism=2

ARG LANG=C.UTF-8
ARG DEBIAN_FRONTEND=noninteractive

ARG BUILD_PACKAGES="autoconf automake build-essential git libtool patch"
ARG DEV_PACKAGES="libcap-ng-dev libjemalloc-dev libssl-dev libunbound-dev systemtap-sdt-dev"
ARG REQUIRED_PACKAGES="curl iproute2 jq libjemalloc2 libunbound8 procps"

ARG DESTDIR=/build

FROM python:3.13-slim-bullseye@sha256:9bf6829d24e9304305ec87973c3b73a94a347019ce0b21994eeb5101dba7c08e AS builder

ARG ovn_version
ARG ovs_version
ARG make_parallelism

ARG LANG
ARG DEBIAN_FRONTEND

ARG BUILD_PACKAGES
ARG DEV_PACKAGES

ARG DESTDIR

COPY --chown=root files/*.patch /

#
# ovn depends on specific ovs commits, so we SHOULD NOT check out from ovs but use the submodule
#
RUN set -eux ; \
    apt-get update; \
    apt-get install -y --no-install-recommends \
        ${BUILD_PACKAGES} \
        ${DEV_PACKAGES}

#
# ovn depends on specific ovs commits, so we SHOULD NOT check out from ovs but use the submodule
#
RUN set -eux ; \
    git clone -b ${ovn_version} --depth 1 https://github.com/ovn-org/ovn.git /ovn; \
    cd /ovn; \
    ./boot.sh; \
    git submodule update --init; \
    #
    # This just builds the needed ovs libs for ovn. We do not actually install it here as we want a specific version later
    #
    cd /ovn/ovs; \
    ./boot.sh; \
    ./configure \
        --enable-usdt-probes \
        --localstatedir=/var \
        --prefix=/usr \
        --sysconfdir=/etc \
        CFLAGS="-g -O2 -march=broadwell" \
        LIBS=-ljemalloc; \
    gmake -j ${make_parallelism}; \
    #
    cd /ovn; \
    patch -p 1 < /increase_mac_binding_delay.patch; \
    patch -p 1 < /feat-add-bfd_chassis-engine.patch; \
    patch -p 1 < /feat-add-nat_addresses-engine.patch; \
    patch -p 1 < /controller-Fix-deletion-of-container-parent-port.patch; \
    patch -p 1 < /controller-Handle-postponed-ports-claims.patch; \
    patch -p 1 < /controller-Handle-postponed-ports-release.patch; \
    patch -p 1 < /revert-controller-Properly-handle-localnet-flows-in-.patch; \
    ./configure \
        --enable-usdt-probes \
        --localstatedir=/var \
        --prefix=/usr \
        --sysconfdir=/etc \
        CFLAGS="-g -O2 -march=broadwell" \
        LIBS=-ljemalloc; \
    gmake -j ${make_parallelism}; \
    make -j ${make_parallelism} install DESTDIR=${DESTDIR}; \
    #
    # Now lets build and install ovs for real (for the ovsdbs)
    #
    cd /ovn/ovs; \
    git checkout ${ovs_version}; \
    ./boot.sh; \
    ./configure \
        --enable-usdt-probes \
        --localstatedir=/var \
        --prefix=/usr \
        --sysconfdir=/etc \
        CFLAGS="-g -O2 -march=broadwell" \
        LIBS=-ljemalloc; \
    gmake -j ${make_parallelism}; \
    #
    # This is needed as "old" build fails
    # https://packaging.python.org/en/latest/tutorials/packaging-projects/#generating-distribution-archives
    pip3 install --upgrade build; \
    make -j ${make_parallelism} install python-sdist DESTDIR=${DESTDIR}; \
    cp python/dist/ovs-*.tar.gz "${DESTDIR}/"; \
    #
    cd /; \
    #
    mkdir -p "${DESTDIR}/bin"; \
    cp /ovn/utilities/docker/start-ovn "${DESTDIR}/bin/start-ovn"


FROM python:3.13-slim-bullseye@sha256:9bf6829d24e9304305ec87973c3b73a94a347019ce0b21994eeb5101dba7c08e

ARG LANG
ARG DEBIAN_FRONTEND

ARG REQUIRED_PACKAGES

ARG DESTDIR

COPY --from=builder ${DESTDIR}/ /
COPY --chown=root files/*.sh /
COPY --chown=root files/*.py /

RUN set -eux ; \
    apt-get update; \
    apt-get install -y --no-install-recommends \
        ${REQUIRED_PACKAGES}; \
    apt-get clean; \
    rm -rf /var/lib/apt/lists/*; \
    pip3 install --no-cache-dir /ovs-*.tar.gz; \
    rm -rf /ovs-*.tar.gz; \
    ovs-appctl --version; \
    ovn-appctl --version
